﻿using UnityEngine;

public class SceneRoot : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private StickerCustomizationPopupView stickerCustomizationPopupView;

	private CachedResourcesLoader cachedResourcesLaoder = new CachedResourcesLoader();

	private void Start()
	{
		string mouthsData = cachedResourcesLaoder.LoadTextFromTextAsset(ResourcesPaths.MOUTHS_DATA_PATH);
		StickerCustomizationPopupModel stickerCustomizationPopupModel = JsonUtility.FromJson<StickerCustomizationPopupModel>(mouthsData);

		int selectedMouthId = stickerCustomizationPopupModel.GetDataWithIndex(0).Id;
		stickerCustomizationPopupView.SetViewModel(new StickerCustomizationPopupViewModel(stickerCustomizationPopupModel, cachedResourcesLaoder, selectedMouthId));
	}
}
