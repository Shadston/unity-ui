﻿using UnityEngine;

public class ResourcesLoader
{
	public virtual Sprite LoadSprite(string spritePath)
	{
		return Resources.Load<Sprite>(spritePath);
	}

	public virtual string LoadTextFromTextAsset(string textAssetPath)
	{
		return Resources.Load<TextAsset>(textAssetPath).text;
	}
}
