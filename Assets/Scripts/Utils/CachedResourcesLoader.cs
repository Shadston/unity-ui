﻿using System.Collections.Generic;
using UnityEngine;

public class CachedResourcesLoader : ResourcesLoader
{
	private Dictionary<string, object> resources = new Dictionary<string, object>();

	public override Sprite LoadSprite(string spritePath)
	{
		if (resources.ContainsKey(spritePath))
			return ((Sprite)resources[spritePath]);

		Sprite sprite = base.LoadSprite(spritePath);
		resources.Add(spritePath, sprite);
		return sprite;
	}

	public override string LoadTextFromTextAsset(string textAssetPath)
	{
		if (resources.ContainsKey(textAssetPath))
			return ((string) resources[textAssetPath]);

		string textAssetText = base.LoadTextFromTextAsset(textAssetPath);
		resources.Add(textAssetPath, textAssetText);
		return textAssetText;
	}
}
