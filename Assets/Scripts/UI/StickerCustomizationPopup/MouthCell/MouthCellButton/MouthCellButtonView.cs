﻿using UnityEngine.UI;
using UnityEngine;
using UniRx;

[RequireComponent(typeof(Button))]
public class MouthCellButtonView : MonoBehaviour
{
	private CompositeDisposable disposeBag = new CompositeDisposable();
    private Button cellButton;

	private void Awake()
    {
        cellButton = GetComponent<Button>();
	}

    public void SetViewModel(MouthCellButtonViewModel viewModel)
    {
		disposeBag.Add(cellButton.OnClickAsObservable().Subscribe(viewModel.ButtonClickObserver));
    }

	private void OnDestroy()
	{
		disposeBag.Clear();
	}
}
