﻿using UnityEngine.UI;
using UnityEngine;
using UniRx;

public class MouthCellView : MonoBehaviour
{
	[Header("Mouth Cell Elements")]
	[SerializeField] private MouthCellButtonView mouthCellButton;
	[SerializeField] private Text mouthNameLabel;
	[SerializeField] private Image mouthIconImage;
	[SerializeField] private GameObject selectedTick;

	private MouthCellViewModel viewModel;
	private CompositeDisposable disposeBag = new CompositeDisposable();

	public void SetViewModel(MouthCellViewModel viewModel)
	{
		disposeBag.Clear();

		this.viewModel = viewModel;

		disposeBag.Add(viewModel.MouthActiveObservable.Subscribe(selectedTick.SetActive));
		mouthCellButton.SetViewModel(viewModel.CreateAndSetUpMouthCellButtonViewModel());

		SetMouthCellLook();
	}

	private void SetMouthCellLook()
	{
		mouthNameLabel.text = viewModel.Name;
		mouthIconImage.sprite = viewModel.Icon;
	}

	private void OnDestroy()
	{
		disposeBag.Clear();
	}
}
