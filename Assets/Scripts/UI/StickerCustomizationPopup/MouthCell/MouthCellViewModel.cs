﻿using UnityEngine;
using System;
using UniRx;

public class MouthCellViewModel : IDisposable
{
	public string Name {  get { return mouthCellModel.Name; } }
	public Sprite Icon { get { return cachedResourcesLoader.LoadSprite(mouthCellModel.IconPath); } }

	public IObservable<bool> MouthActiveObservable { get { return mouthActive.AsObservable(); } } 
	public IObserver<bool> MouthActiveObserver { get { return mouthActive; } }
	private readonly BehaviorSubject<bool> mouthActive = new BehaviorSubject<bool>(false);

	private readonly Subject<Unit> selectionIntent = new Subject<Unit>();
	public IObservable<Unit> SelectionIntent {  get { return selectionIntent.AsObservable(); } }

	private readonly CachedResourcesLoader cachedResourcesLoader;
	private readonly MouthCellModel mouthCellModel;

	private MouthCellButtonViewModel mouthCellButtonViewModel;
	private CompositeDisposable disposeBag = new CompositeDisposable();

	public MouthCellViewModel(MouthCellModel mouthCellModel, CachedResourcesLoader cachedResourcesLoader)
    {
        this.mouthCellModel = mouthCellModel;
		this.cachedResourcesLoader = cachedResourcesLoader;
    }

    public MouthCellButtonViewModel CreateAndSetUpMouthCellButtonViewModel()
    {
		if (mouthCellButtonViewModel != null)
			return mouthCellButtonViewModel;
		
		mouthCellButtonViewModel = new MouthCellButtonViewModel();
	    disposeBag.Add(mouthCellButtonViewModel.ButtonClickStream.Subscribe(selectionIntent.OnNext));

		return mouthCellButtonViewModel;
    }

	public void Dispose()
	{
		disposeBag.Clear();
	}
}
