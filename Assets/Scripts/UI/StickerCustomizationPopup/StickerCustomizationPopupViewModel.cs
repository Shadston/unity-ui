﻿using UnityEngine;
using System;
using UniRx;

public class StickerCustomizationPopupViewModel : IDisposable
{
	public int MouthsCount {  get { return stickerCustomizationPopupModel.MouthsDataCount; } }

	public IObservable<string> SelectedMouthName { get { return selectedMouthModel.Select(x => x.Name); } }
	public IObservable<Sprite> SelectedMouthIcon { get { return selectedMouthModel.Select(x => cachedResourcesLoader.LoadSprite(x.IconPath)); } }

	private readonly BehaviorSubject<MouthCellModel> selectedMouthModel;
	private readonly CachedResourcesLoader cachedResourcesLoader;

    private readonly StickerCustomizationPopupModel stickerCustomizationPopupModel;
	private CompositeDisposable disposeBag = new CompositeDisposable();

	public StickerCustomizationPopupViewModel(StickerCustomizationPopupModel stickerCustomizationPopupModel, CachedResourcesLoader cachedResourcesLoader, int selectedMouthId)
    {
		selectedMouthModel = new BehaviorSubject<MouthCellModel>(stickerCustomizationPopupModel.GetDataWithIndex(selectedMouthId));
        this.stickerCustomizationPopupModel = stickerCustomizationPopupModel;
		this.cachedResourcesLoader = cachedResourcesLoader;
	}

    public MouthCellViewModel CreateAndSetUpMouthCellViewModel(int mouthIndex)
    {
		MouthCellModel model = stickerCustomizationPopupModel.GetDataWithIndex(mouthIndex);
		MouthCellViewModel mouthCellViewModel = new MouthCellViewModel(model, cachedResourcesLoader);
	    disposeBag.Add(mouthCellViewModel.SelectionIntent.Subscribe(_ => selectedMouthModel.OnNext(model)));
		disposeBag.Add(selectedMouthModel.Select(x => x.Id == mouthIndex).Subscribe(mouthCellViewModel.MouthActiveObserver));

		return mouthCellViewModel;
    }

	public void Dispose()
	{
		disposeBag.Clear();
	}
}
