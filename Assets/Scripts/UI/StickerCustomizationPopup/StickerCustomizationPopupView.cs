﻿using UnityEngine.UI;
using UnityEngine;
using UniRx;

public class StickerCustomizationPopupView : MonoBehaviour
{
    [Header("Prefabs")]
    [SerializeField] private GameObject mouthCellPrefab;

    [Header("Popup elements")]
    [SerializeField] private Image stickerMouth;
	[SerializeField] private Text mouthLabelName;
    [SerializeField] private Transform mouthCellsContainer;

	private StickerCustomizationPopupViewModel viewModel;
	private CompositeDisposable disposeBag = new CompositeDisposable();

    public void SetViewModel(StickerCustomizationPopupViewModel viewModel)
    {
		disposeBag.Clear();
        this.viewModel = viewModel;

		disposeBag.Add(viewModel.SelectedMouthName.Subscribe(name => mouthLabelName.text = name));
		disposeBag.Add(viewModel.SelectedMouthIcon.Subscribe(icon => stickerMouth.sprite = icon));

		CreateNewMouthsGridWithData();
	}

    private void CreateNewMouthsGridWithData()
    {
		DestroyAlreadyLoadedCells();

		for (int i = 0; i < viewModel.MouthsCount; i++)
            CreateMouthCell(i);
    }

	private void DestroyAlreadyLoadedCells()
	{
		for (int i = 0; i < mouthCellsContainer.childCount; i++)
			Destroy(mouthCellsContainer.GetChild(i).gameObject);
	}

    private void CreateMouthCell(int mouthIndex)
    {
        GameObject mouthCell = Instantiate(mouthCellPrefab, Vector3.zero, Quaternion.identity, mouthCellsContainer);
		mouthCell.GetComponent<MouthCellView>().SetViewModel(viewModel.CreateAndSetUpMouthCellViewModel(mouthIndex));
    }

	private void OnDestroy()
	{
		disposeBag.Clear();
	}
}
